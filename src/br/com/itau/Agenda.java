package br.com.itau;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Agenda {
    private static Map <String, Contato> contatos;
    private static List<Contato> listaContatos;
    private int qtdContatos;

    public Agenda(int qtdPessoa) {
        this.qtdContatos = qtdPessoa;
        contatos = new HashMap<>();
        listaContatos = new ArrayList<>();
    }

    public int getQtdContatos() {

        qtdContatos= listaContatos.size();
        return qtdContatos;
    }


    public static void adicionarContato (Contato contato) throws Exception {

        if(listaContatos.contains(contato)){
          throw new Exception("Esse contato já existe na lista");
        }
        listaContatos.add(contato);

        contatos.put(contato.getEmail(), contato);
        contatos.put(contato.getNumero(), contato);
    }

    public static void removerContato (String email) throws Exception{
        Contato contatoRemover = contatos.get(email);
        if(!listaContatos.contains(contatoRemover)){
            throw new Exception("Esse contato não existe na lista");
        }
        listaContatos.remove(contatoRemover);
    }

    public static void imprimirPeloEmail (String email) throws Exception {
        Contato contatoImprimir = contatos.get(email);
        if(!listaContatos.contains(contatoImprimir)){
            throw new Exception("Esse contato não existe na lista");
        }

            impressaoContato(contatoImprimir);

    }

    public static void imprimirPeloNumero (String numero) throws Exception{
        Contato contatoImprimir = contatos.get(numero);
        if(!listaContatos.contains(contatoImprimir)){
            throw new Exception("Esse contato não existe na lista");
        }
            impressaoContato(contatoImprimir);

    }

    public static void impressaoContato (Contato contatoImprimir){
        System.out.println("\nContato:" + "\n"
                + contatoImprimir.getNome() + "\n"
                + contatoImprimir.getEmail() + "\n"
                + contatoImprimir.getNumero());
    }

    public static List<Contato> getListaContatos() {
        return listaContatos;
    }

    public static void imprimirListaContatos(){
        System.out.println("\n************LISTA DE CONTATOS********************");
        for(int i=0; i< listaContatos.size(); i++){
            System.out.println(listaContatos.get(i).getNome());
        }
    }


}
