package br.com.itau;

public class Main {

    public static void main(String[] args) {
	// write your code here
     try {
         Contato contato1 = new Contato("Ingrid Monalisa de Lima Bicudo", "ingrid.bicudo@itau.com.br", "992554307");
         Contato contato2 = new Contato("Maria Bicudo", "maria.bicudo@itau.com.br", "964374438");
         Agenda agenda = new Agenda(2);

         agenda.adicionarContato(contato1);
         agenda.adicionarContato(contato2);

         agenda.removerContato(contato1.getEmail());

         //agenda.imprimirPeloEmail(contato1.getEmail());
         agenda.imprimirPeloNumero(contato2.getNumero());
     } catch (Exception e){
         e.printStackTrace();
     }

    }
}
